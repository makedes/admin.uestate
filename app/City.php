<?php

namespace App;

use App\Scopes\ScopesTrait;
use Dimsav\Translatable\Translatable;

class City extends Base
{
    use Translatable;
    use ScopesTrait;

    public $translatedAttributes = [
        'name',
        'description',
        'locale',
        'metatags',
        'is_crop',
        'image',
        'data_crop',
        'data_crop_info',
    ];
    protected $fillable = [
        'area_id',
        'region_id',
        'koatuu',
        'ru_name',
        'uk_name',
        'en_name',
        'priority',
        'longitude',
        'latitude',
        'top_latitude',
        'top_longitude',
        'bottom_latitude',
        'bottom_longitude',
    ];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function region()
    {
        return$this->belongsTo(Region::class);
    }
}
