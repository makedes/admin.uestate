<?php

namespace App;

use App\Scopes\ScopesTrait;
use Dimsav\Translatable\Translatable;

class Area extends Base
{
    use Translatable;

    use ScopesTrait;

    public $translatedAttributes = [
        'name',
        'locale',
        'description',
        'metatags',
        'is_crop',
        'image',
        'data_crop',
        'data_crop_info',
    ];
    protected $fillable = [
        'region_id',
        'koatuu',
        'ru_name',
        'uk_name',
        'en_name',
        'longitude',
        'latitude',
        'top_latitude',
        'top_longitude',
        'bottom_latitude',
        'bottom_longitude',
    ];

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
