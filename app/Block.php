<?php

namespace App;

use App\Scopes\ScopesTrait;
use Dimsav\Translatable\Translatable;

class Block extends Base
{
    use Translatable;
    use ScopesTrait;

    public $translatedAttributes = [
        'name',
        'locale',
        'text',
        'image',
        'is_crop',
        'data_crop',
        'data_crop_info'
    ];
    protected $fillable = [
        'published',
        'alias',
    ];

}
