<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'text',
        'locale',
        'image',
        'is_crop',
        'data_crop',
        'data_crop_info'
    ];
}
