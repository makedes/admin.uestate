<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'locale',
        'name',
        'region_id',
        'description',
        'metatags',
        'is_crop',
        'image',
        'data_crop',
        'data_crop_info',
    ];
}
