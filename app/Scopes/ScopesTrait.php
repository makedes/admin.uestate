<?php
namespace App\Scopes;

use App\Helpers\FormLang;
use Illuminate\Database\Eloquent\Builder;

trait ScopesTrait
{
    public function scopePublished($query)
    {
        return $query->where('published',1);
    }

    public function scopeManyLike($query, $fields,$value)
    {
        $value = mb_strtolower($value);

        $fields_trans = [];

        foreach ($fields as $k=>$field){
            if (isset($this->translatedAttributes) && in_array($field,$this->translatedAttributes)){

                $fields_trans[] = $field;

            }else{
                if ($k == 0)
                    $query->where($field,'like','%'.$value.'%');
                else
                    $query->orWhere($field,'like','%'.$value.'%');
            }

        }

        if (method_exists($this,'translations') && !empty($fields_trans)){
            $query->orWhereHas('translations', function (Builder $query) use ($fields_trans, $value) {
                foreach ($fields_trans as $key=>$v){
                    if ($key == 0) $query->where($this->getTranslationsTable().'.'.$v,'like','%'.$value.'%');
                    else $query->orWhere($this->getTranslationsTable().'.'.$v,'like','%'.$value.'%');
                }

                $query->where($this->getTranslationsTable().'.'.$this->getLocaleKey(), FormLang::getCurrentLang());

            });
        }


        return $query;
    }

    /**
     * This scope filters results by checking the translation fields.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $key
     * @param string                                $value
     * @param string                                $locale
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function scopeOrWhereTranslationLike(Builder $query, $key, $value, $locale = null)
    {
        return $query->whereHas('translations', function (Builder $query) use ($key, $value, $locale) {
            $query->orWhere($this->getTranslationsTable().'.'.$key, 'LIKE', $value);
            if ($locale) {
                $query->orWhere($this->getTranslationsTable().'.'.$this->getLocaleKey(), 'LIKE', $locale);
            }
        });
    }

    public static function boot()
    {
        parent::boot();

    }


}