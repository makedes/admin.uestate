<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'locale',
        'name',
        'area_id',
        'description',
        'metatags',
        'is_crop',
        'image',
        'data_crop',
        'data_crop_info',
    ];
}
