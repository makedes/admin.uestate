<?php

namespace App;

use App\Scopes\ScopesTrait;
use Dimsav\Translatable\Translatable;

class Region extends Base
{
    use Translatable;
    use ScopesTrait;

    public $translatedAttributes = [
        'name',
        'locale',
        'description',
        'metatags',
        'is_crop',
        'image',
        'data_crop',
        'data_crop_info',
    ];
    protected $fillable = [
        'koatuu',
        'ru_name',
        'uk_name',
        'en_name',
        'longitude',
        'latitude',
        'top_latitude',
        'top_longitude',
        'bottom_latitude',
        'bottom_longitude',
    ];

    public function areas()
    {
        return $this->hasMany(Area::class);
    }

    public function cities()
    {
        return $this->hasMany(Region::class);
    }
}
