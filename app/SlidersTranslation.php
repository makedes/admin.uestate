<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlidersTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'locale',
        'name',
        'description',
    ];
}
