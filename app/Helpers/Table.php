<?php namespace App\Helpers;

/**
 * Created by PhpStorm.
 * User: yura
 * Date: 08.03.16
 * Time: 14:02
 */

class Table {

    public $dataModel;

    public $isMultiLang = false;

    public $columns = [];

    public $controller = '';

    public $by_position = false;

    public $param = [];

    public $actionGroupDelete = true;

    public $isAjax = true;

    public $actionGroupActive = true;

    public $actionGroupDeselect = true;

    public $actionGroupRecovery = false;

    public $imageTable = false;

    public function getView($data = null,$param = [])
    {

        $this->param = $param;

        if (!\Request::ajax()){
            $data['data'] = [];
            $data['header'] = $this->columns;
            $data['by_position'] = $this->by_position;
            $data['active'] = $this->actionGroupActive;
            $data['isAjax'] = $this->isAjax;
            $data['de_active'] = $this->actionGroupDeselect;
            $data['delete'] = $this->actionGroupDelete;
            $data['recovery'] = $this->actionGroupRecovery;
            $data['isImage'] = $this->imageTable;
            return view('table.index')->with(['data'=>$data,'controller'=>$this->controller]);
        }else{
            if($data === null){
                $data = $this->getData();
            }

        }

        $data['header'] = $this->columns;
        $data['by_position'] = $this->by_position;
        $data['active'] = $this->actionGroupActive;
        $data['isAjax'] = $this->isAjax;
        $data['de_active'] = $this->actionGroupDeselect;
        $data['delete'] = $this->actionGroupDelete;
        $data['recovery'] = $this->actionGroupRecovery;
        $data['isImage'] = $this->imageTable;





        if ($this->isAjax && !$this->by_position){
            $tableData = \Request::all();

            $search = [];

            foreach ($tableData['columns'] as $value){
                if ($value['searchable']=='true')
                    $search[] = $value['data'];
            }
            if (!empty($this->param))
                $data['iTotalRecords'] = $this->dataModel->where($this->param)->count();
            else
                $data['iTotalRecords'] = $this->dataModel->count();

            if (is_numeric($tableData['search']['value'])){
                if (!empty($this->param))
                    $item = $this->dataModel->where($param)->find($tableData['search']['value']);
                else
                    $item = $this->dataModel->find($tableData['search']['value']);
                if (!empty($item)){
                    $data['iTotalDisplayRecords'] = 1;
                }else{
                    $data['iTotalDisplayRecords'] = 0;
                }

            }else{
                if (!empty($this->param))
                    $data['iTotalDisplayRecords'] = $this->dataModel->manyLike($search,$tableData['search']['value'])->where($this->param)->count();
                else
                    $data['iTotalDisplayRecords'] = $this->dataModel->manyLike($search,$tableData['search']['value'])->count();
            }

            foreach ($data['data'] as $key=>$value){
                $data['data'][$key]['check'] = '<td><input type="checkbox" class="styled"></td>';
                foreach ($this->columns as $k=>$v){
                    $data['data'][$key][$k] = view('table.render.'.$v['type'],['aRow'=>$value,'aColumns'=>$data['header'],'key'=>$k])->render();
                }
            }

            return $data;
        }


        return view('table.index')->with(['data'=>$data,'controller'=>$this->controller]);
    }

    private function getData()
    {
        if ($this->isAjax && !$this->by_position){
            $tableData = \Request::all();

            $search = [];

            foreach ($tableData['columns'] as $value){
                if ($value['searchable']=='true')
                    $search[] = $value['data'];
            }

            $order = $tableData['order'][0];
            if ($tableData['columns'][$order['column']]['orderable']=='true')
                $order['column'] = $tableData['columns'][$order['column']]['data'];
            else{
                $name_col_order = '';
                foreach ($tableData['columns'] as $val){
                    if ($val['orderable']=='true'){
                        $name_col_order = $val['data'];
                        break;
                    }
                }
                $order['column'] = $name_col_order == '' ? 'id' : $name_col_order;
            }

            if($this->isMultiLang){
                $tempo_locale = \App::getLocale();
                \App::setLocale(FormLang::getCurrentLang());
                if (is_numeric($tableData['search']['value'])){
                    if (!empty($this->param))
                        $item = $this->dataModel->where($this->param)->find($tableData['search']['value']);
                    else
                        $item = $this->dataModel->find($tableData['search']['value']);

                    if (!empty($item)){
                        $item = $item->toArray();
                        $data['data'][0] = $item;
                    }else{
                        $data['data'] = [];
                    }

                }
                else
                    if (!empty($this->param))
                        $data['data'] = $this->dataModel->manyLike($search,$tableData['search']['value'])->where($this->param)->orderBy($order['column'], $order['dir'])->limit($tableData['length'])->offset($tableData['start'])->get()->toArray();
                    else
                        $data['data'] = $this->dataModel->manyLike($search,$tableData['search']['value'])->orderBy($order['column'], $order['dir'])->limit($tableData['length'])->offset($tableData['start'])->get()->toArray();
                \App::setLocale($tempo_locale);
            }else{
                $data['data'] = $this->dataModel->manyLike($search,$tableData['search']['value'])->orderBy($order['column'], $order['dir'])->limit($tableData['length'])->offset($tableData['start'])->get()->toArray();
            }
        }else{
            if($this->isMultiLang){
                $tempo_locale = \App::getLocale();
                \App::setLocale(FormLang::getCurrentLang());
                if($this->by_position)
                    $data['data'] = $this->dataModel->orderBy('position', 'asc')->get()->toArray();
                else
                    $data['data'] = $this->dataModel->orderBy('created_at', 'desc')->get()->toArray();
                \App::setLocale($tempo_locale);
            }else{
                $data['data'] = $this->dataModel->all()->toArray();
            }
        }


        $data['header'] = $this->columns;
        $data['by_position'] = $this->by_position;
        $data['active'] = $this->actionGroupActive;
        $data['de_active'] = $this->actionGroupDeselect;
        $data['delete'] = $this->actionGroupDelete;
        $data['recovery'] = $this->actionGroupRecovery;
        $data['isImage'] = $this->imageTable;

        return $data;
    }

}