<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'locale',
        'name',
        'city_id',
        'description',
        'metatags',
        'is_crop',
        'image',
        'data_crop',
        'data_crop_info',
    ];
}
