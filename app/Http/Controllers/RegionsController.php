<?php

namespace App\Http\Controllers;

use App\Helpers\Table;
use App\Http\Requests\RegionsRequest;
use App\Region;
use Illuminate\Http\Request;

class RegionsController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->title = trans('app.regions');
        $this->model = new Region();
        $this->controller = 'regions';

        parent::__construct($request);
    }

    public function index()
    {
        $table = new Table();

        $table->dataModel = $this->model;

        $table->isMultiLang = $this->isMultiLang;

        $table->actionGroupActive =false;
        $table->actionGroupDelete = false;
        $table->actionGroupDeselect = false;

        $table->controller = $this->controller;

        $table->columns = [
            'edit'=>[
                'order'=>false,
                'type'=>'edit',
                'width' => 10,
            ],
            'id'=>[
                'order'=>true,
                'type'=>'default',
                'width'=> 10
            ],


            'uk_name'=>[
                'order'=>true,
                "searchable" => true,
                'type'=>'default',
            ],

            'ru_name'=>[
                'order'=>true,
                "searchable" => true,
                'type'=>'default',
            ],

            'en_name'=>[
                'order'=>true,
                "searchable" => true,
                'type'=>'default',
            ],

        ];

        if (\Request::ajax()){
            return $table->getView();
        }
        $table = $table->getView();

        return view($this->controller.'.index',compact('table'));

    }


    public function postStore(RegionsRequest $request)
    {
        $data = $request->all();

        if($this->isMultiLang)
            $data = prepareDataToAdd($this->model->translatedAttributes,$data);

        $content = $this->model->create($data);

        return redirectApp(
            Route('edit_'.$this->controller,['id'=>$content->id]),
            '302',trans('app.item was created'),trans('app.Saved'),'success'
        );
    }

    public function postUpdate(RegionsRequest $request, $id)
    {
        $content = $this->model->find($id);
        $data = $request->all();

        foreach($data as $name=>$item){
            if(!in_array($name,$this->model->translatedAttributes))
                $content->{$name} = $item;
            else
                $content->translate($data['locale'])->{$name} =  $item;
        }

        $content->save();

        return redirectApp(
            Route('edit_'.$this->controller,['id'=>$content->id]),
            '302',trans('app.data saved'),trans('app.Saved'),'success'
        );
    }
}
