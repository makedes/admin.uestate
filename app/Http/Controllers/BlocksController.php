<?php

namespace App\Http\Controllers;

use App\Block;
use App\Content;
use App\Helpers\FormLang;
use App\Helpers\Main;
use App\Helpers\Table;
use Illuminate\Http\Request;

use App\Http\Requests;

class BlocksController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->title = trans('app.blocks');
        $this->model = new Block();
        $this->controller = 'blocks';

        $this->columns = [
            'edit'=>[
                'order'=>false,
                'type'=>'edit',
                "searchable" => false,
                'width' => 10,
            ],
            'id'=>[
                'order'=>true,
                'type'=>'default',
                "searchable" => false,
                'width'=> 10
            ],
            'published'=>[
                'order'=>true,
                'type'=>'bool',
                "searchable" => false,
                'width'=> 10
            ],
            'alias'=>[
                'order'=>true,
                "searchable" => true,
                'type'=>'default',
            ],
            'name'=>[
                'order'=>true,
                "searchable" => true,
                'type'=>'default',
            ],
            'created_at'=>[
                'order'=>true,
                "searchable" => false,
                'type'=>'default',
            ],
            'delete'=>[
                'order'=>false,
                "searchable" => false,
                'type'=>'delete',
                'width'=> 10
            ],
        ];

        parent::__construct($request);
    }

    /**
     * Main action for current part of admin application
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $table = new Table();
        $table->dataModel = $this->model;
        $table->isMultiLang  = $this->isMultiLang;

        $table->controller = $this->controller;
        $table->columns  = $this->columns;

        if (\Request::ajax()){
            return $table->getView();
        }
        $table = $table->getView();

        return view($this->controller.'.index',['table'=>$table]);
    }

    public function postStore(Requests\BlocksRequest $request)
    {
        $data = $request->all();

        if($this->isMultiLang)
            $data = prepareDataToAdd($this->model->translatedAttributes,$data);

        $content = $this->model->create($data);

        return redirectApp(
            Route('edit_'.$this->controller,['id'=>$content->id]),
            '302',trans('app.item was created'),trans('app.Saved'),'success'
        );
    }

    public function postUpdate(Requests\BlocksRequest $request, $id)
    {
        $content = $this->model->find($id);
        $data = $request->all();

        foreach($data as $name=>$item){
            if(!in_array($name,$this->model->translatedAttributes))
                $content->{$name} = $item;
            else
                $content->translate($data['locale'])->{$name} =  $item;
        }

        $content->save();

        return redirectApp(
            Route('edit_blocks',['id'=>$content->id]),
            '302',trans('app.data saved'),trans('app.Saved'),'success'
        );
    }
}
