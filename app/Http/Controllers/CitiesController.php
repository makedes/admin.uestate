<?php

namespace App\Http\Controllers;

use App\Area;
use App\City;
use App\Helpers\Table;
use App\Http\Requests\CitiesRequest;
use App\Http\Requests\RegionsRequest;
use App\Region;
use Illuminate\Http\Request;

class CitiesController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->title = trans('app.cities');
        $this->model = new City();
        $this->controller = 'cities';

        parent::__construct($request);
    }

    public function index()
    {

        $table = new Table();

        $table->dataModel = $this->model;

        $table->isMultiLang = $this->isMultiLang;

        $table->controller = $this->controller;

        $table->columns = [
            'edit'=>[
                'order'=>false,
                'type'=>'edit',
                "className" => "text-center",
                "searchable" => false,
                'width' => 10,
            ],
            'id'=>[
                'order'=>true,
                'type'=>'default',
                "searchable" => false,

                'width'=> 10
            ],


            'uk_name'=>[
                'order'=>true,
                "searchable" => true,
                'type'=>'default',
            ],

            'ru_name'=>[
                'order'=>true,
                "searchable" => true,
                'type'=>'default',
            ],

            'en_name'=>[
                'order'=>true,
                "searchable" => true,
                'type'=>'default',
            ],

            'type'=>[
                'order'=>true,
                "searchable" => true,
                'type'=>'type_city',
            ],
            'delete'=>[
                'order'=>false,
                "className" => "text-center",
                "searchable" => false,
                'type'=>'delete',
                'width'=> 10
            ],

        ];

        if (\Route::current()->parameter('region_id') !== null){
            $region = Region::findOrFail(\Route::current()->parameter('region_id'));
            $param = ['region_id'=>\Route::current()->parameter('region_id')];

            if (\Route::current()->parameter('area_id') !== null){
                $area = Area::findOrFail(\Route::current()->parameter('area_id'));
                $param = array_merge($param,['area_id'=>\Route::current()->parameter('area_id')]);
            }
        }

        else
            $param = [];

        if (\Request::ajax()){
            return $table->getView(null,$param);
        }
        $table = $table->getView(null,$param);

        return view($this->controller.'.index',compact('table'));

    }


    public function postStore(CitiesRequest $request)
    {
        $data = $request->all();

        if($this->isMultiLang)
            $data = prepareDataToAdd($this->model->translatedAttributes,$data);

        $content = $this->model->create($data);

        return redirectApp(
            Route('edit_'.$this->controller,['id'=>$content->id]),
            '302',trans('app.item was created'),trans('app.Saved'),'success'
        );
    }

    public function postUpdate(CitiesRequest $request, $id)
    {
        $content = $this->model->find($id);
        $data = $request->all();

        foreach($data as $name=>$item){
            if(!in_array($name,$this->model->translatedAttributes))
                $content->{$name} = $item;
            else
                $content->translate($data['locale'])->{$name} =  $item;
        }

        $content->save();

        return redirectApp(
            Route('edit_'.$this->controller,['id'=>$content->id]),
            '302',trans('app.data saved'),trans('app.Saved'),'success'
        );
    }

    public function postRegions(Request $request)
    {
        $regions = Region::manyLike(['ru_name','uk_name','en_name'],$request->get('term'))->get();

        $result = [];

        foreach ($regions as $region ){
            $result['items'][] = ['id'=>$region->id,'name'=>$region->name];
        }

        return $result;
    }

    public function postAreas(Request $request)
    {
        $regions = Area::manyLike(['ru_name','uk_name','en_name'],$request->get('term'))->where('region_id',$request->get('region_id'))->get();

        $result = [];

        foreach ($regions as $region ){
            $result['items'][] = ['id'=>$region->id,'name'=>$region->name];
        }

        return $result;
    }
}
