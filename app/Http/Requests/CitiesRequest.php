<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CitiesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = \Route::current()->parameter('id');
        if($id !== null) return \Auth::user()->can(['regions-add-delete','regions-edit']);
        return \Auth::user()->can('regions-add-delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [

            'name' => [
                'required:cities',
                'max:255'
            ],
            'koatuu' => [
                'required:cities',
                'max:10',
                'min:10',
            ],
            'type' => [
                'required:cities',
            ],
            'region_id' => [
                'required:cities',
            ],

            'en_name' => [
                'required:cities',
                'max:255'
            ],
            'uk_name' => [
                'required:cities',
                'max:255'
            ],
            'ru_name' => [
                'required:cities',
                'max:255'
            ]

        ];
    }
}
