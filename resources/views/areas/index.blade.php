@extends('layout.default.main')

@section('central')

    <div class="col-lg-6">
        <div class="form-group form-group-material">
            <label class="control-label">{{ trans('app.region') }}</label>
            <div class="input-group">
                <select  data-placeholder="{{ trans('app.select region') }}" class="select region-select">
                    <option></option>
                    @if(Route::current()->parameter('region_id') !== null)
                        <option value="{{ Route::current()->parameter('region_id') }}" selected>{{ \App\Region::find( Route::current()->parameter('region_id'))->name }}</option>
                    @endif
                </select>
                <div class="input-group-btn">
                    <button type="button" id="to-region" class="btn bg-green-800 ">{{ trans('app.go') }} <i class=" icon-arrow-right15"></i> </button>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            $('.region-select').select2({
                ajax: {
                    url : '{{ route('areas_get_regions') }}',
                    dataType: "json",
                    type: "POST",
                    data: function (params) {
                        var queryParameters = {
                            term: params.term
                        };
                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });

            $('#to-region').on('click',function () {
                window.location.href = '{{ route($controller) }}'+'/'+ $('.region-select').val();
            });
        });

    </script>


    {!! $table !!}

@endsection