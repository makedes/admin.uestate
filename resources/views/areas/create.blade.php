@extends('layout.default.main')

@section('central')

        <form class="form-horizontal" method="post" onsubmit="return Main.formSubmit(this);" action="{{ route('add_new_'.$controller) }}">
            @include($controller.'.form')
        </form>

@endsection