@if(Route::current()->getName() == 'edit_'.$controller)
    @permission($controller.'-view')
    <div class="btn-group">
        <a target="_blank" href="{{ route('cities',['region_id'=>\App\Area::find(Route::current()->parameter('id'))->region_id,'area_id'=>Route::current()->parameter('id')]) }}" class="btn btn-primary" ><i class=" icon-city"></i> {{ trans('app.cities of area') }}</a>
    </div>
    @endpermission
@endif