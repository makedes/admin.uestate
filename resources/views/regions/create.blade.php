@extends('layout.default.main')

@section('central')

        <form class="form-horizontal" method="post" onsubmit="return Main.formSubmit(this);" action="{{ route($controller.'_add') }}">
            @include($controller.'.form')
        </form>

@endsection