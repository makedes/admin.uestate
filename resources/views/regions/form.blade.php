<fieldset class="content-group">

    {!! viewTextInput('name',@$translation->name ,trans('app.name')) !!}

    {!! viewTextInput('koatuu',@$content->koatuu ,trans('app.koatuu'),isset($content->koatuu)) !!}

    {!! viewCKEDitor('description',@$translation->description,trans('app.description')) !!}

    @if(@$translation->image)
        {!! imageField('image',$translation->image,trans('app.image'),$translation->is_crop) !!}
    @else
        {!! imageField('image','',trans('app.image')) !!}
    @endif

    <h3>{{ trans('app.fields for search') }}</h3>

    {!! viewTextInput('uk_name',@$content->uk_name,trans('app.uk_name')) !!}

    {!! viewTextInput('ru_name',@$content->ru_name,trans('app.ru_name')) !!}

    {!! viewTextInput('en_name',@$content->en_name,trans('app.en_name')) !!}



    <div class="row">
        <div class="col-lg-4">
            <h5>{{ trans('app.center map') }}</h5>

            {!! viewTextInput('latitude',@$content->latitude,trans('app.latitude')) !!}

            {!! viewTextInput('longitude',@$content->longitude,trans('app.longitude')) !!}

        </div>
        <div class="col-lg-4">
            <h5>{{ trans('app.top map') }}</h5>
            {!! viewTextInput('top_latitude',@$content->top_latitude,trans('app.latitude')) !!}

            {!! viewTextInput('top_longitude',@$content->top_longitude,trans('app.longitude')) !!}

        </div>
        <div class="col-lg-4">
            <h5>{{ trans('app.bottom map') }}</h5>
            {!! viewTextInput('bottom_latitude',@$content->bottom_latitude,trans('app.latitude')) !!}

            {!! viewTextInput('bottom_longitude',@$content->bottom_longitude,trans('app.longitude')) !!}

        </div>
    </div>


    @if(@$content->longitude && @$content->latitude)

        <div class="panel panel-flat">
            <div class="panel-body">

                <div class="map-container map-symbol-custom" id="map"></div>
            </div>
        </div>

        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key={{ env('MAPS_KEY') }}"
                type="text/javascript"></script>
        <script src="/js/markerwithlabel_packed.js"
                type="text/javascript"></script>        <script src="/js/custom-map.js"
                type="text/javascript"></script>

        <script>
            var _latitude = {{ $content->latitude }};
            var _longitude = {{ $content->longitude }};

            google.maps.event.addDomListener(window, 'load', initSubmitMap(_latitude,_longitude));
            function initSubmitMap(_latitude,_longitude){
                var mapCenter = new google.maps.LatLng(_latitude,_longitude);
                var mapOptions = {
                    scrollwheel: false

                };
                var mapElement = document.getElementById('map');
                var map = new google.maps.Map(mapElement, mapOptions);
                var marker = new MarkerWithLabel({
                    position: mapCenter,
                    map: map,
                    icon: '/img/map_marker.png',
                    labelAnchor: new google.maps.Point(50, 0),
                    draggable: true
                });

                var points = [
                    {
                        lat :  {{ $content->top_latitude }},
                        lon: {{ $content->top_longitude }}
                    },
                    {
                        lat : {{ $content->bottom_latitude }},
                        lon: {{ $content->bottom_longitude }}
                    },
                    {
                        lat : {{ $content->latitude }},
                        lon: {{ $content->longitude }}
                    }
                ];




                google.maps.event.addListener(marker, "mouseup", function (event) {
                    $('#latitude').val( this.position.lat() );
                    $('#longitude').val( this.position.lng() );
                });

                var marker_top = new MarkerWithLabel({
                    position: new google.maps.LatLng( {{ $content->top_latitude }}, {{ $content->top_longitude }}),
                    map: map,
                    icon: '/img/map_top_marker.png',
                    labelAnchor: new google.maps.Point(50, 0),
                    draggable: true
                });

                google.maps.event.addListener(marker_top, "mouseup", function (event) {
                    $('#top_latitude').val( this.position.lat() );
                    $('#top_longitude').val( this.position.lng() );
                });

                var marker_bottom = new MarkerWithLabel({
                    position: new google.maps.LatLng( {{ $content->bottom_latitude }}, {{ $content->bottom_longitude }}),
                    map: map,
                    icon: '/img/map_bottom_marker.png',
                    labelAnchor: new google.maps.Point(50, 0),
                    draggable: true
                });

                google.maps.event.addListener(marker_bottom, "mouseup", function (event) {
                    $('#bottom_latitude').val( this.position.lat() );
                    $('#bottom_longitude').val( this.position.lng() );
                });

                var bounds = new google.maps.LatLngBounds();

                for (j in points){
                    pos = new google.maps.LatLng(points[j]['lat'],points[j]['lon']);
                    bounds.extend(pos);
                }
                map.fitBounds(bounds);

            }



        </script>





    @endif
</fieldset>

{!! Form::hidden('locale',\App\Helpers\FormLang::getCurrentLang()) !!}

<div class="text-right">
    <button type="submit" class="btn btn-primary">{{ trans('app.submit') }} <i class="icon-arrow-right14 position-right"></i></button>
</div>



