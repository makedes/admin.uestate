@if(Route::current()->getName() == 'edit_'.$controller)
    @permission($controller.'-view')
        <div class="btn-group">
            <a target="_blank" href="{{ route('areas',['region_id'=>Route::current()->parameter('id')]) }}" class="btn btn-primary" ><i class="icon-location3"></i>{{ trans('app.areas of region') }}</a>
            <a target="_blank" href="{{ route('cities',['region_id'=>Route::current()->parameter('id')]) }}" class="btn btn-primary" ><i class=" icon-city"></i> {{ trans('app.cities of region') }}</a>
        </div>
    @endpermission
@endif