@permission($controller.'-add-delete')
    <div class="btn-group">
        <a href="{{ route($controller.'_add') }}" class="btn btn-primary" ><i class=" icon-add position-left"></i>{{ trans('app.add_'.$controller) }}</a>
    </div>
@endpermission