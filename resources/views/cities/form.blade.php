<fieldset class="content-group">

    {!! viewTextInput('name',@$translation->name ,trans('app.name')) !!}

    {!! viewTextInput('koatuu',@$content->koatuu ,trans('app.koatuu'),isset($content->koatuu)) !!}

    <div class="form-group ">
        <label class="control-label col-lg-2 text-semibold">{{ trans('app.type') }}</label>
        <div class="col-lg-10">
            <select name="type"  cf="true" class="select" id="type">
                <option>{{ trans('app.select')}}</option>
                @foreach(Config::get('admin.city_type') as $key => $value)
                    <option @if(@$content->type ==$key) selected @endif value="{{ $key }}">{{ trans('app.'.$value['name']) }}</option>
                @endforeach
            </select>
            <div class="form-control-feedback" id="type-error-icon">
                <i class="icon-cancel-circle2"></i>
            </div>
            <span class="help-block" id="type-error"></span>
        </div>
    </div>


    <div class="form-group ">
        <label class="control-label col-lg-2 text-semibold">{{ trans('app.region') }}</label>
        <div class="col-lg-10">

            <select id="region_id" name="region_id" data-placeholder="{{ trans('app.select region') }}" class="select region-select">
                <option></option>
                @if(@$content->region_id)
                    <option value="{{ $content->region_id }}" selected>{{ $content->region->name }}</option>
                @endif
            </select>
            <span class="help-block" id="region_id-error"></span>
            <div class="form-control-feedback" id="region_id-error-icon">
                <i class="icon-cancel-circle2"></i>
            </div>


        </div>
    </div>



    <div class="form-group select-area-block">
        <label class="control-label col-lg-2 text-semibold">{{ trans('app.area') }}</label>
        <div class="col-lg-10">

            <select id="area_id" name="area_id" data-placeholder="{{ trans('app.select area') }}" class="select area-select">
                <option></option>
                @if(@$content->area_id)
                    <option value="{{ $content->area_id }}" selected>{{ $content->area->name }}</option>
                @endif
            </select>
            <span class="help-block" id="area_id-error"></span>
            <div class="form-control-feedback" id="area_id-error-icon">
                <i class="icon-cancel-circle2"></i>
            </div>


        </div>
    </div>



    <script>
        $(document).ready(function () {
            if(!$('.region-select').val()){
                $('.select-area-block').hide();
            }

            $('.region-select').on('change',function () {
                if ($(this).val()){
                    $('.select-area-block').show();
                }
            });
            $('.region-select').select2({
                ajax: {
                    url : '{{ route('areas_get_regions') }}',
                    dataType: "json",
                    type: "POST",
                    data: function (params) {
                        var queryParameters = {
                            term: params.term
                        };
                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                }
            });

            $('.area-select').select2({
                ajax: {
                    url : '{{ route('cities_get_areas') }}',
                    dataType: "json",
                    type: "POST",
                    data: function (params) {
                        var queryParameters = {
                            term: params.term,
                            region_id: $('.region-select').val()
                        };
                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });


        });

    </script>

    {!! viewCKEDitor('description',@$translation->description,trans('app.description')) !!}

    @if(@$translation->image)
        {!! imageField('image',$translation->image,trans('app.image'),$translation->is_crop) !!}
    @else
        {!! imageField('image','',trans('app.image')) !!}
    @endif

    <h3>{{ trans('app.fields for search') }}</h3>
    @if(isset($content->region_id))
        <div class="form-group ">
            <label class="control-label col-lg-2 text-semibold">{{ trans('app.region') }}</label>
            <div class="col-lg-10">
                <a target="_blank" href="{{ route('edit_regions',['id'=>$content->region_id]) }}"><span class="label label-primary">{{ $content->region->name }}</span></a>
            </div>
        </div>
    @endif
    @if(isset($content->area_id))
        <div class="form-group ">
            <label class="control-label col-lg-2 text-semibold">{{ trans('app.area') }}</label>
            <div class="col-lg-10">
                <a target="_blank" href="{{ route('edit_areas',['id'=>$content->area_id]) }}"><span class="label label-success">{{ $content->area->name }}</span></a>
            </div>
        </div>
    @endif
    {!! viewTextInput('uk_name',@$content->uk_name,trans('app.uk_name')) !!}

    {!! viewTextInput('ru_name',@$content->ru_name,trans('app.ru_name')) !!}

    {!! viewTextInput('en_name',@$content->en_name,trans('app.en_name')) !!}



    <div class="row">
        <div class="col-lg-4">
            <h5>{{ trans('app.center map') }}</h5>

            {!! viewTextInput('latitude',@$content->latitude,trans('app.latitude')) !!}

            {!! viewTextInput('longitude',@$content->longitude,trans('app.longitude')) !!}

        </div>
        <div class="col-lg-4">
            <h5>{{ trans('app.top map') }}</h5>
            {!! viewTextInput('top_latitude',@$content->top_latitude,trans('app.latitude')) !!}

            {!! viewTextInput('top_longitude',@$content->top_longitude,trans('app.longitude')) !!}

        </div>
        <div class="col-lg-4">
            <h5>{{ trans('app.bottom map') }}</h5>
            {!! viewTextInput('bottom_latitude',@$content->bottom_latitude,trans('app.latitude')) !!}

            {!! viewTextInput('bottom_longitude',@$content->bottom_longitude,trans('app.longitude')) !!}

        </div>
    </div>


    @if(@$content->longitude && @$content->latitude)

        <div class="panel panel-flat">
            <div class="panel-body">

                <div class="map-container map-symbol-custom" id="map"></div>
            </div>
        </div>

        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key={{ env('MAPS_KEY') }}"
                type="text/javascript"></script>
        <script src="/js/markerwithlabel_packed.js"
                type="text/javascript"></script>        <script src="/js/custom-map.js"
                type="text/javascript"></script>

        <script>
            var _latitude = {{ $content->latitude }};
            var _longitude = {{ $content->longitude }};

            google.maps.event.addDomListener(window, 'load', initSubmitMap(_latitude,_longitude));
            function initSubmitMap(_latitude,_longitude){
                var mapCenter = new google.maps.LatLng(_latitude,_longitude);
                var mapOptions = {
                    scrollwheel: false

                };
                var mapElement = document.getElementById('map');
                var map = new google.maps.Map(mapElement, mapOptions);
                var marker = new MarkerWithLabel({
                    position: mapCenter,
                    map: map,
                    icon: '/img/map_marker.png',
                    labelAnchor: new google.maps.Point(50, 0),
                    draggable: true
                });

                var points = [
                    {
                        lat :  {{ $content->top_latitude ? $content->top_latitude : $content->latitude + 0.01 }},
                        lon: {{ $content->top_longitude ? $content->top_longitude : $content->longitude + 0.01 }}
                    },
                    {
                        lat : {{ $content->bottom_latitude ? $content->bottom_latitude : $content->latitude - 0.01 }},
                        lon: {{ $content->bottom_longitude ? $content->bottom_longitude : $content->longitude - 0.01 }}
                    },
                    {
                        lat : {{ $content->latitude }},
                        lon: {{ $content->longitude }}
                    }
                ];




                google.maps.event.addListener(marker, "mouseup", function (event) {
                    $('#latitude').val( this.position.lat() );
                    $('#longitude').val( this.position.lng() );
                });

                var marker_top = new MarkerWithLabel({
                    position: new google.maps.LatLng( {{ $content->top_latitude ? $content->top_latitude : $content->latitude + 0.01 }}, {{ $content->top_longitude ? $content->top_longitude : $content->longitude + 0.01 }}),
                    map: map,
                    icon: '/img/map_top_marker.png',
                    labelAnchor: new google.maps.Point(50, 0),
                    draggable: true
                });

                google.maps.event.addListener(marker_top, "mouseup", function (event) {
                    $('#top_latitude').val( this.position.lat() );
                    $('#top_longitude').val( this.position.lng() );
                });

                var marker_bottom = new MarkerWithLabel({
                    position: new google.maps.LatLng( {{ $content->bottom_latitude ? $content->bottom_latitude : $content->latitude - 0.01 }}, {{ $content->bottom_longitude ? $content->bottom_longitude : $content->longitude - 0.01 }}),
                    map: map,
                    icon: '/img/map_bottom_marker.png',
                    labelAnchor: new google.maps.Point(50, 0),
                    draggable: true
                });

                google.maps.event.addListener(marker_bottom, "mouseup", function (event) {
                    $('#bottom_latitude').val( this.position.lat() );
                    $('#bottom_longitude').val( this.position.lng() );
                });

                var bounds = new google.maps.LatLngBounds();

                for (j in points){
                    pos = new google.maps.LatLng(points[j]['lat'],points[j]['lon']);
                    bounds.extend(pos);
                }
                map.fitBounds(bounds);

            }



        </script>





    @endif
</fieldset>

{!! Form::hidden('locale',\App\Helpers\FormLang::getCurrentLang()) !!}

<div class="text-right">
    <button type="submit" class="btn btn-primary">{{ trans('app.submit') }} <i class="icon-arrow-right14 position-right"></i></button>
</div>



