<div class="form-group ">
    <label class="control-label col-lg-2 text-semibold">{{ $label }}</label>
    <div class="col-lg-10">
        {!! Form::textarea($id, $value ,['class'=>'ckeditor form-control','id'=>$id,'cf'=>'true']) !!}
        <div class="form-control-feedback" id="{{ $id }}-error-icon">
            <i class="icon-cancel-circle2"></i>
        </div>
        <span class="help-block" id="{{ $id }}-error"></span>
    </div>
</div>
<script>
    var editor = CKEDITOR.replace( '{{ $id }}',{
        filebrowserBrowseUrl : '/elfinder/ckeditor',
        contentsCss : '{{ env('CKEDITOR_CSS') }}',
        language: '{{ LaravelLocalization::getCurrentLocale() }}'

    } );
</script>