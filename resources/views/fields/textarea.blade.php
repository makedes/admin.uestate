<div class="form-group ">
    <label class="control-label col-lg-2 text-semibold">{{ $label }}</label>
    <div class="col-lg-10">
        @if($readonly)
        {!! Form::textarea($id, $value ,['class'=>'form-control','id'=>$id,'cf'=>'true','readonly'=>'readonly']) !!}
        @else
        {!! Form::textarea($id, $value ,['class'=>'form-control','id'=>$id,'cf'=>'true']) !!}
        @endif
        <div class="form-control-feedback" id="{{ $id }}-error-icon">
            <i class="icon-cancel-circle2"></i>
        </div>
        <span class="help-block" id="{{ $id }}-error"></span>
    </div>
</div>