<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->string('koatuu',10);
            $table->string('ru_name');
            $table->string('uk_name');
            $table->string('en_name');
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->string('top_latitude')->nullable();
            $table->string('top_longitude')->nullable();
            $table->string('bottom_latitude')->nullable();
            $table->string('bottom_longitude')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('districts');
    }
}
