<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('structure_id')->unsigned()->nullable();
            $table->boolean('published')->default(true);
            $table->string('alias');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('block_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('block_id')->unsigned();
            $table->string('locale');
            $table->string('name');
            $table->text('text')->nullable();
            $table->string('image',1024)->nullable();
            $table->boolean('is_crop')->default(false);
            $table->string('data_crop',1024)->nullable();
            $table->string('data_crop_info',1024)->nullable();
            $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('block_translations');
        Schema::drop('blocks');
    }
}
